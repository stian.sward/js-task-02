const fs = require('fs');
const os = require('os');
const http = require('http');

// Print menu
console.log(`Choose an option:
1. Read package.json
2. Display OS info
3. Start HTTP server`);

// Wait for and return user input
const query = require('cli-interact');
var input;
while (true) {
    input = query.getInteger('Type a number: ');
    if (![1, 2, 3].includes(input)) {
        console.log('Invalid input. Try again');
    } else {
        break;
    }
}

// Execute choice
switch (input) {
    case 1:
        readJSON();
        break;
    case 2:
        displayOSInfo();
        break;
    case 3:
        startHTTPServer();
        break;
    default:
        console.log("Unknown error. This call should've been unreachable.");
        break;
}

// Read and print package.json
function readJSON() {
    console.log('\n--- PACKAGE.JSON ---\n');
    fs.readFile(__dirname + '/package.json', 'utf-8', (err, content) => {
        console.log(content);
    });
}

// Display OS information
function displayOSInfo() {
    console.log('\n--- OS INFO ---');
    console.log('Operating System:', os.version());
    console.log('Release:', os.release());
    console.log('Platform:', os.platform());
    console.log('Architecture:', os.arch());
    console.log('CPU:', os.cpus()[0].model);
    console.log('Cores:', os.cpus().length);
    console.log('System memory:', (os.totalmem() / 1024 / 1024 / 1024).toFixed(2), 'GB');
    console.log('Free memory:', (os.freemem / 1024 / 1024 / 1024).toFixed(2), 'GB');
    console.log('User:', os.userInfo().username);
    const uptime = os.uptime();
    const days = Math.floor(uptime / (3600*24));
    const hours = Math.floor(uptime % (3600*24) / 3600);
    const minutes = Math.floor(uptime % 3600 / 60);
    const seconds = Math.floor(uptime % 60);
    console.log(`Total system uptime: ${days}:${hours}:${minutes}:${seconds}`);
}

// Start HTTP Server
function startHTTPServer() {
    let port = 5050;
    console.log('Starting HTTP server...')
    const server = http.createServer((req, res) => {
        if (req.url === "/") {
            res.write('Hello World!');
            res.end();
        }
    });
    server.listen(port);
    console.log('Listeing on port', port);
}